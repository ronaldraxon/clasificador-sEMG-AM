function inicializarRutasBase(rutaInicial)
%INICIALIZARRUTASBASE Summary of this function goes here
%   Agrega las carpetas al conjunto de rutas
rutaRecursos ='/resources';
rutaCore ='/matlab/core';
rutaGUI ='/matlab/gui';
rutaPrototipos ='/matlab/prototype';
addpath(strcat(rutaInicial, rutaRecursos));
addpath(strcat(rutaInicial, rutaCore));
addpath(strcat(rutaInicial, rutaGUI));
addpath(strcat(rutaInicial, rutaPrototipos));
end
