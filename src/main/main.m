%%%                     SISTEMAS INTELIGENTES                           %%%
%%%               CLASIFICADOR DE MOVIMIENTOS DE EMG                    %%%
%%%                RONALD RODRÍGUEZ - JHONATHAN SORA                    %%%
%%%                        21 DE MAYO  2018                             %%%
%%%                     APRENDIZAJE DE MAQUINA                          %%%

clc;clear;close all;
load 'carateristicas.mat'

pos = [1 12;2 12;3 12;4 12;5 12;6 12;7 12;8 12;9 12;10 12;11 12;12 12;1 4;5 8;9 12];
%%
for j = 1:15
    for i = 1:10
        [x_train,x_test] = train_test(caracteristicas);
        
        Entrenamiento = x_train(:,pos(j,1):pos(j,2));
        Prueba = x_test(:,pos(j,1):pos(j,2));
        
        salidasEntrenamiento = x_train(:,13);
        salidasPruebas = x_test(:,13);
        
        SVM = fitcsvm(Entrenamiento,salidasEntrenamiento,...
            'KernelFunction', 'polynomial', ...'linear','polynomial','gaussian',
            'PolynomialOrder', [2], ...
            'KernelScale','auto', ...0.87,...14,...'auto', ...
            'BoxConstraint', 1, ...
            'Standardize', true, ...
            'Solver','L1QP',... % 'ISDA','L1QP','SMO';
            'ClassNames', [-1; 1]);
        
        Accuracy_entrenamiento(i,j) = (1 - resubLoss(SVM, 'LossFun', 'ClassifError'))*100;
        
        Y_train = predict(SVM,Prueba);
        Accuracy_prueba(i,j) = (1-(sum(salidasPruebas~=Y_train)/length(Y_train)))*100;
        j,i
    end
    
end
%%
 %filename = 'datos_gauss2_ISDA.xlsx';
 filename = 'datos_cuadratico2_L1QP.xlsx';
 %filename = 'datos_gauss2_SMO.xlsx';

xlswrite(filename,Accuracy_entrenamiento,1)
xlswrite(filename,Accuracy_prueba,2)
