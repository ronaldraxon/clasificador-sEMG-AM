function [x_train,x_test] = train_test(caracteristicas)
%% Entrenamiento - Prueba
m = length(caracteristicas);
val = randperm(m);
entrenamiento = round(length(val)*0.7);
x_train = zeros(185,13);
x_test = zeros(79,13);

%%% MATRIZ DE ENTRENAMIENTO %%%
for i=1:entrenamiento
    pos_m1 = val(1,i);
    x_train(i,:) = caracteristicas(pos_m1,:);
end

%%% MARTIZ TEST %%%
for i=entrenamiento+1:m
    pos_m1 = val(1,i);
    x_test(i-entrenamiento,:) = caracteristicas(pos_m1,1:13); 
end
end