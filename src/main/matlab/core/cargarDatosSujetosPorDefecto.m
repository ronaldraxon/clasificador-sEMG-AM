function [datosDefecto] = cargarDatosSujetosPorDefecto()
%CARGARDATOSSUJETOSDEFECTO Summary of this function goes here
%   Inicializa la carga de datos de los sujetos por defecto
S1 = load('S1_E1_A1_5-6.mat');
S2 = load('S2_E1_A1_5-6.mat');
S3 = load('S3_E1_A1_5-6.mat');
S4 = load('S4_E1_A1_5-6.mat');
S5 = load('S5_E1_A1_5-6.mat');
S6 = load('S6_E1_A1_5-6.mat');
S7 = load('S7_E1_A1_5-6.mat');
S8 = load('S8_E1_A1_5-6.mat');
S9 = load('S9_E1_A1_5-6.mat');
S10 = load('S10_E1_A1_5-6.mat');
S11 = load('S11_E1_A1_5-6.mat');
S12 = load('S12_E1_A1_5-6.mat');
S13 = load('S13_E1_A1_5-6.mat');
S14 = load('S14_E1_A1_5-6.mat');
S15 = load('S18_E1_A1_5-6.mat');
S16 = load('S19_E1_A1_5-6.mat');
S17 = load('S20_E1_A1_5-6.mat');
S18 = load('S22_E1_A1_5-6.mat');
S19 = load('S28_E1_A1_5-6.mat');
S20 = load('S35_E1_A1_5-6.mat');
S21 = load('S36_E1_A1_5-6.mat');
S22 = load('S38_E1_A1_5-6.mat');
[datosDefecto] = [S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,S14,S15,...
                  S16,S17,S18,S19,S20,S21,S22];
end

