function matrizMovimientosCodificados = codificarCampoDeMovimiento(matrizDeDatos,indiceCampoMovimiento)
%CODIFICARCAMPODEMOVIMIENTOS Summary of this function goes here
%Realiza la codificaci�n de el campo de movimiento en 1 y 0 a partir de la
%cantidad de valores. Para cada valor se genera un campo cuyos valores son
%los n�meros 1 y 0, en donde 1 es presencia de movimiento y 0 ausencia.
%campo 1 mano abierta, campo 2 mano cerrada
tamanioMatrizDeDatos= size(matrizDeDatos);
cantidadDeFilas = tamanioMatrizDeDatos(1);
campoMovimiento1 = zeros(cantidadDeFilas,1);
campoMovimiento2 = zeros(cantidadDeFilas,1);
    for i = 1:cantidadDeFilas
        if matrizDeDatos(i,indiceCampoMovimiento) == 1;
            campoMovimiento1(i) = 1;
        else
            campoMovimiento2(i) = 1;
        end
    end
[matrizResultante] =  horzcat(matrizDeDatos(:,1:12),campoMovimiento1,campoMovimiento2);
matrizMovimientosCodificados = matrizResultante;
end

