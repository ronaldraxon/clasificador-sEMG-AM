function [ output_args ] = entrenamientoRed( entradas,salidas,entrenamiento, cantidadNeuronasOcultas)
%ENTRENAMIENTORED Summary of this function goes here
%   Detailed explanation goes here

x = entradas';
t = salidas';

% Choose a Training Function
% For a list of all training functions type: help nntrain
% 'trainlm' is usually fastest.
% 'trainbr' takes longer but may be better for challenging problems.
% 'trainscg' uses less memory. Suitable in low memory situations.
%trainFcn = entrenamiento;  % Scaled conjugate gradient backpropagation.

% Create a Pattern Recognition Network
hiddenLayerSize = cantidadNeuronasOcultas;
%net =feedforwardnet(12,'trainscg');
net = patternnet(hiddenLayerSize);
net.trainFcn = entrenamiento;
% Choose Input and Output Pre/Post-Processing Functions
% For a list of all processing functions type: help nnprocess
net.input.processFcns = {'removeconstantrows','mapminmax'};
net.output.processFcns = {'removeconstantrows','mapminmax'};

% Setup Division of Data for Training, Validation, Testing
% For a list of all data division functions type: help nndivide
net.divideFcn = 'dividerand';  % Divide data randomly
net.divideMode = 'sample';  % Divide up every sample
net.divideParam.trainRatio = 70/100;
net.divideParam.valRatio = 15/100;
net.divideParam.testRatio = 15/100;

% Choose a Performance Function
% For a list of all performance functions type: help nnperformance
net.performFcn = 'crossentropy';  % Cross-Entropy

% Choose Plot Functions
% For a list of all plot functions type: help nnplot
net.plotFcns = {'plotperform','plottrainstate','ploterrhist', ...
    'plotconfusion', 'plotroc'};

% Train the Network
[net,tr] = train(net,x,t,'useParallel','yes');

% Test the Network
y = net(x,'useParallel','yes');
e = gsubtract(t,y);

performance = perform(net,t,y);
tind = vec2ind(t);
yind = vec2ind(y);
percentErrors = sum(tind ~= yind)/numel(tind);

% Recalculate Training, Validation and Test Performance
trainTargets = t .* tr.trainMask{1};
valTargets = t .* tr.valMask{1};
testTargets = t .* tr.testMask{1};
trainPerformance = perform(net,trainTargets,y);
valPerformance = perform(net,valTargets,y);
testPerformance = perform(net,testTargets,y);
[c1,cm1,ind1,per1] = confusion(trainTargets,y);
[c2,cm2,ind2,per2] = confusion(valTargets,y);
[c3,cm3,ind3,per3] = confusion(testTargets,y);
[c4,cm4,ind4,per4] = confusion(t,y);

fila = horzcat(c1,c2,c3,c4);
[ output_args ] = fila;
end

