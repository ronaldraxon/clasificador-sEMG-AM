function [out] = extraerCaracteristicas(datosPorDefecto)
%clc;clear;close all
S1 = datosPorDefecto(1);
S2 = datosPorDefecto(2);
S3 = datosPorDefecto(3);
S4 = datosPorDefecto(4);
S5 = datosPorDefecto(5);
S6 = datosPorDefecto(6);
S7 = datosPorDefecto(7);
S8 = datosPorDefecto(8);
S9 = datosPorDefecto(9);
S10 = datosPorDefecto(10);
S11 = datosPorDefecto(11);
S12 = datosPorDefecto(12); 
S13 = datosPorDefecto(13); 
S14 = datosPorDefecto(14); 
S15 = datosPorDefecto(15); 
S16 = datosPorDefecto(16); 
S17 = datosPorDefecto(17); 
S18 = datosPorDefecto(18); 
S19 = datosPorDefecto(19); 
S20 = datosPorDefecto(20); 
S21 = datosPorDefecto(21); 
S22 = datosPorDefecto(22); 

%% selecci�n de mov 5,6 y canales 10,11
Fs = 100;   %F.SAMPLE DEL EMG
F = 130000;

emg_s1 = S1.sujeto(1:F,10:11); % tomo el canal 10 y 11
emg_s2 = S2.sujeto(1:F,10:11);
emg_s3 = S3.sujeto(1:F,10:11);
emg_s4 = S4.sujeto(1:F,10:11);
emg_s5 = S5.sujeto(1:F,10:11);
emg_s6 = S6.sujeto(1:F,10:11);
emg_s7 = S7.sujeto(1:F,10:11);
emg_s8 = S8.sujeto(1:F,10:11);
emg_s9 = S9.sujeto(1:F,10:11);
emg_s10 = S10.sujeto(1:F,10:11);
emg_s11 = S11.sujeto(1:F,10:11);
emg_s12 = S12.sujeto(1:F,10:11); 
emg_s13 = S13.sujeto(1:F,10:11); 
emg_s14 = S14.sujeto(1:F,10:11); 
emg_s15 = S15.sujeto(1:F,10:11); 
emg_s16 = S16.sujeto(1:F,10:11); 
emg_s17 = S17.sujeto(1:F,10:11); 
emg_s18 = S18.sujeto(1:F,10:11); 
emg_s19 = S19.sujeto(1:F,10:11); 
emg_s20 = S20.sujeto(1:F,10:11); 
emg_s21 = S21.sujeto(1:F,10:11); 
emg_s22 = S22.sujeto(1:F,10:11);

st_c =  S1.sujeto(1:F,1); % stimulus

emg_s = double(horzcat(emg_s1,emg_s2,emg_s3,emg_s4,emg_s5,emg_s6,emg_s7,...
    emg_s8,emg_s9,emg_s10,emg_s11,emg_s12,emg_s13,emg_s14,emg_s15,emg_s16,...
    emg_s17,emg_s18,emg_s19,emg_s20,emg_s21,emg_s22)); % concateno todos los EMG

%% Lowpass filter 5 Hz 2 Orden
h = designfilt('lowpassiir','FilterOrder',2,'PassbandFrequency',5,'PassbandRipple',0.2,'SampleRate',Fs);
emg_f = filtfilt(h,emg_s);

%% Divisi�n de movimientos
st = ([st_c(1:F/2,1); 6+st_c(1:F/2,1)]);

picos = diff(st);
mov = find(picos==1);
mov = ([ones(1); mov]);

for i=1:6
    uno = mov(i)+2000;
    dos = uno + 7000;
    
    %%%S1_M1 %%%
    s1_ch9_m1(i,:) = emg_f(uno:dos,1);
    s1_ch10_m1(i,:) = emg_f(uno:dos,2);
    %%%S2_M1 %%%
    s2_ch9_m1(i,:) = emg_f(uno:dos,3);
    s2_ch10_m1(i,:) = emg_f(uno:dos,4);
    %%%S3_M1 %%%
    s3_ch9_m1(i,:) = emg_f(uno:dos,5);
    s3_ch10_m1(i,:) = emg_f(uno:dos,6);
    %%%S4_M1 %%%
    s4_ch9_m1(i,:) = emg_f(uno:dos,7);
    s4_ch10_m1(i,:) = emg_f(uno:dos,8);
    %%%S5_M1 %%%
    s5_ch9_m1(i,:) = emg_f(uno:dos,9);
    s5_ch10_m1(i,:) = emg_f(uno:dos,10);
    %%%S6_M1 %%%
    s6_ch9_m1(i,:) = emg_f(uno:dos,11);
    s6_ch10_m1(i,:) = emg_f(uno:dos,12);
    %%%S7_M1 %%%
    s7_ch9_m1(i,:) = emg_f(uno:dos,13);
    s7_ch10_m1(i,:) = emg_f(uno:dos,14);
    %%%S8_M1 %%%
    s8_ch9_m1(i,:) = emg_f(uno:dos,15);
    s8_ch10_m1(i,:) = emg_f(uno:dos,16);
    %%%S9_M1 %%%
    s9_ch9_m1(i,:) = emg_f(uno:dos,17);
    s9_ch10_m1(i,:) = emg_f(uno:dos,18);
    %%%S10_M1 %%%
    s10_ch9_m1(i,:) = emg_f(uno:dos,19);
    s10_ch10_m1(i,:) = emg_f(uno:dos,20);
    %%%S11_M1 %%%
    s11_ch9_m1(i,:) = emg_f(uno:dos,21);
    s11_ch10_m1(i,:) = emg_f(uno:dos,22);
    
    %%%S12_M1 %%%
    s12_ch9_m1(i,:) = emg_f(uno:dos,23);
    s12_ch10_m1(i,:) = emg_f(uno:dos,24);
    %%%S13_M1 %%%
    s13_ch9_m1(i,:) = emg_f(uno:dos,25);
    s13_ch10_m1(i,:) = emg_f(uno:dos,26);
    %%%S14_M1 %%%
    s14_ch9_m1(i,:) = emg_f(uno:dos,27);
    s14_ch10_m1(i,:) = emg_f(uno:dos,28);
    %%%S15_M1 %%%
    s15_ch9_m1(i,:) = emg_f(uno:dos,29);
    s15_ch10_m1(i,:) = emg_f(uno:dos,30);
    %%%S16_M1 %%%
    s16_ch9_m1(i,:) = emg_f(uno:dos,31);
    s16_ch10_m1(i,:) = emg_f(uno:dos,32);
    %%%S17_M1 %%%
    s17_ch9_m1(i,:) = emg_f(uno:dos,33);
    s17_ch10_m1(i,:) = emg_f(uno:dos,34);
    %%%S18_M1 %%%
    s18_ch9_m1(i,:) = emg_f(uno:dos,35);
    s18_ch10_m1(i,:) = emg_f(uno:dos,36);
    %%%S19_M1 %%%
    s19_ch9_m1(i,:) = emg_f(uno:dos,37);
    s19_ch10_m1(i,:) = emg_f(uno:dos,38);
    %%%S20_M1 %%%
    s20_ch9_m1(i,:) = emg_f(uno:dos,39);
    s20_ch10_m1(i,:) = emg_f(uno:dos,40);
    %%%S21_M1 %%%
    s21_ch9_m1(i,:) = emg_f(uno:dos,41);
    s21_ch10_m1(i,:) = emg_f(uno:dos,42);
    %%%S22_M1 %%%
    s22_ch9_m1(i,:) = emg_f(uno:dos,43);
    s22_ch10_m1(i,:) = emg_f(uno:dos,44);
end

for i=7:12
    uno = mov(i)+2500;
    dos= uno + 7000;
    
   %%%S1_M2 %%%
    s1_ch9_m2(i-6,:) = emg_f(uno:dos,1);
    s1_ch10_m2(i-6,:) = emg_f(uno:dos,2);
    %%%S2_M2 %%%
    s2_ch9_m2(i-6,:) = emg_f(uno:dos,3);
    s2_ch10_m2(i-6,:) = emg_f(uno:dos,4);
    %%%S3_M2 %%%
    s3_ch9_m2(i-6,:) = emg_f(uno:dos,5);
    s3_ch10_m2(i-6,:) = emg_f(uno:dos,6);
    %%%S4_M2 %%%
    s4_ch9_m2(i-6,:) = emg_f(uno:dos,7);
    s4_ch10_m2(i-6,:) = emg_f(uno:dos,8);
    %%%S5_M2 %%%
    s5_ch9_m2(i-6,:) = emg_f(uno:dos,9);
    s5_ch10_m2(i-6,:) = emg_f(uno:dos,10);
    %%%S6_M2 %%%
    s6_ch9_m2(i-6,:) = emg_f(uno:dos,11);
    s6_ch10_m2(i-6,:) = emg_f(uno:dos,12);
    %%%S7_M2 %%%
    s7_ch9_m2(i-6,:) = emg_f(uno:dos,13);
    s7_ch10_m2(i-6,:) = emg_f(uno:dos,14);
    %%%S8_M2 %%%
    s8_ch9_m2(i-6,:) = emg_f(uno:dos,15);
    s8_ch10_m2(i-6,:) = emg_f(uno:dos,16);
    %%%S9_M2 %%%
    s9_ch9_m2(i-6,:) = emg_f(uno:dos,17);
    s9_ch10_m2(i-6,:) = emg_f(uno:dos,18);
    %%%S10_M2 %%%
    s10_ch9_m2(i-6,:) = emg_f(uno:dos,19);
    s10_ch10_m2(i-6,:) = emg_f(uno:dos,20);
    %%%S11_M2 %%%
    s11_ch9_m2(i-6,:) = emg_f(uno:dos,21);
    s11_ch10_m2(i-6,:) = emg_f(uno:dos,22);
    
    %%%S12_M2 %%%
    s12_ch9_m2(i-6,:) = emg_f(uno:dos,23);
    s12_ch10_m2(i-6,:) = emg_f(uno:dos,24);
    %%%S13_M2 %%%
    s13_ch9_m2(i-6,:) = emg_f(uno:dos,25);
    s13_ch10_m2(i-6,:) = emg_f(uno:dos,26);
    %%%S14_M2 %%%
    s14_ch9_m2(i-6,:) = emg_f(uno:dos,27);
    s14_ch10_m2(i-6,:) = emg_f(uno:dos,28);
    %%%S15_M2 %%%
    s15_ch9_m2(i-6,:) = emg_f(uno:dos,29);
    s15_ch10_m2(i-6,:) = emg_f(uno:dos,30);
    %%%S16_M2 %%%
    s16_ch9_m2(i-6,:) = emg_f(uno:dos,31);
    s16_ch10_m2(i-6,:) = emg_f(uno:dos,32);
    %%%S17_M2 %%%
    s17_ch9_m2(i-6,:) = emg_f(uno:dos,33);
    s17_ch10_m2(i-6,:) = emg_f(uno:dos,34);
    %%%S18_M2 %%%
    s18_ch9_m2(i-6,:) = emg_f(uno:dos,35);
    s18_ch10_m2(i-6,:) = emg_f(uno:dos,36);
    %%%S19_M2 %%%
    s19_ch9_m2(i-6,:) = emg_f(uno:dos,37);
    s19_ch10_m2(i-6,:) = emg_f(uno:dos,38);
    %%%S20_M2 %%%
    s20_ch9_m2(i-6,:) = emg_f(uno:dos,39);
    s20_ch10_m2(i-6,:) = emg_f(uno:dos,40);
    %%%S21_M2 %%%
    s21_ch9_m2(i-6,:) = emg_f(uno:dos,41);
    s21_ch10_m2(i-6,:) = emg_f(uno:dos,42);
    %%%S22_M2 %%%
    s22_ch9_m2(i-6,:) = emg_f(uno:dos,43);
    s22_ch10_m2(i-6,:) = emg_f(uno:dos,44);
end

%% MATRIZ DE DATOS
m5_1 = vertcat(s1_ch9_m1,s2_ch9_m1,s3_ch9_m1,s4_ch9_m1,s5_ch9_m1,s6_ch9_m1,...
    s7_ch9_m1,s8_ch9_m1,s9_ch9_m1,s10_ch9_m1,s11_ch9_m1,s12_ch9_m1,s13_ch9_m1,...
    s14_ch9_m1,s15_ch9_m1,s16_ch9_m1,s17_ch9_m1,s18_ch9_m1,s19_ch9_m1,...
    s20_ch9_m1,s21_ch9_m1,s22_ch9_m1);

m5_2 = vertcat(s1_ch10_m1,s2_ch10_m1,s3_ch10_m1,s4_ch10_m1,s5_ch10_m1,s6_ch10_m1,...
    s7_ch10_m1,s8_ch10_m1,s9_ch10_m1,s10_ch10_m1,s11_ch10_m1,s12_ch10_m1,...
    s13_ch10_m1,s14_ch10_m1,s15_ch10_m1,s16_ch10_m1,s17_ch10_m1,s18_ch10_m1,...
    s19_ch10_m1,s20_ch10_m1,s21_ch10_m1,s22_ch10_m1);

m6_1 = vertcat(s1_ch9_m2,s2_ch9_m2,s3_ch9_m2,s4_ch9_m2,s5_ch9_m2,s6_ch9_m2,...
    s7_ch9_m2,s8_ch9_m2,s9_ch9_m2,s10_ch9_m2,s11_ch9_m2,s12_ch9_m2,s13_ch9_m2,...
    s14_ch9_m2,s15_ch9_m2,s16_ch9_m2,s17_ch9_m2,s18_ch9_m2,s19_ch9_m2,s20_ch9_m2,...
    s21_ch9_m2,s22_ch9_m2);

m6_2 = vertcat(s1_ch10_m2,s2_ch10_m2,s3_ch10_m2,s4_ch10_m2,s5_ch10_m2,s6_ch10_m2,...
    s7_ch10_m2,s8_ch10_m2,s9_ch10_m2,s10_ch10_m2,s11_ch10_m2,s12_ch10_m2,...
    s13_ch10_m2,s14_ch10_m2,s15_ch10_m2,s16_ch10_m2,s17_ch10_m2,s18_ch10_m2,...
    s19_ch10_m2,s20_ch10_m2,s21_ch10_m2,s22_ch10_m2);

%% MATRIZ CARACTERISTICAS
[F_train,L_train] = size (m5_1);
for i=1:F_train
    
    %%%CARACTERISTICAS TIEMPO
    mc1(i,1) = (trapz(abs(m5_1(i,:))))/264 ;% Mean Absolute Value MAV
    mc1(i,2) = (trapz(abs(m5_2(i,:))))/264 ;% Mean Absolute Value MAV
    mc2(i,1) = (trapz(abs(m6_1(i,:))))/264 ;% Mean Absolute Value MAV
    mc2(i,2) = (trapz(abs(m6_2(i,:))))/264 ;% Mean Absolute Value MAV
    
    mc1(i,3) = rms(m5_1(i,:)); % Root Mean Square RMS
    mc1(i,4) = rms(m5_2(i,:)); % Root Mean Square RMS
    mc2(i,3) = rms(m6_1(i,:)); % Root Mean Square RMS
    mc2(i,4) = rms(m6_2(i,:)); % Root Mean Square RMS
    
    mc1(i,5) = trapz(abs(m5_1(i,:)));% Integrated EMG IEMG
    mc1(i,6) = trapz(abs(m5_2(i,:)));% Integrated EMG IEMG
    mc2(i,5) = trapz(abs(m6_1(i,:)));% Integrated EMG IEMG
    mc2(i,6) = trapz(abs(m6_2(i,:)));% Integrated EMG IEMG
    
    %%% CARACTERISTICAS EN FRECUENCIA %%%
    [pxx51,f51]= pwelch(m5_1(i,:),150,50,150,Fs);%%% PSD
    [pxx52,f52]= pwelch(m5_2(i,:),150,50,150,Fs);%%% PSD
    [pxx61,f61]= pwelch(m6_1(i,:),150,50,150,Fs);%%% PSD
    [pxx62,f62]= pwelch(m6_2(i,:),150,50,150,Fs);%%% PSD
    
    mc1(i,7) = meanfreq(pxx51,f51); % Mean frequency MNF
    mc1(i,8) = meanfreq(pxx52,f52); % Mean frequency MNF
    mc2(i,7) = meanfreq(pxx61,f61); % Mean frequency MNF
    mc2(i,8) = meanfreq(pxx62,f62); % Mean frequency MNF

    mc1(i,9) = trapz(pxx51);% PSD
    mc1(i,10) = trapz(pxx52);% PSD
    mc2(i,9) = trapz(pxx61);% PSD
    mc2(i,10) = trapz(pxx62);% PSD
    
    mc1(i,11) = bandpower(m5_1(i,:)); % Total power TTP
    mc1(i,12) = bandpower(m5_2(i,:)); % Total power TTP
    mc2(i,11) = bandpower(m6_1(i,:)); % Total power TTP
    mc2(i,12) = bandpower(m6_2(i,:)); % Total power TTP
end

mc = ([mc1 ; mc2]);
m = length(mc);

y_t = [-ones(m/2,1);ones(m/2,1)]; % Etiqueta del dato
out = horzcat(mc,y_t);


% %% MATRIZ DE ENTRENAMIENTO %%%
% val = randperm(m);
% entrenamiento = round(length(val)*0.7);
% 
% for i=1:entrenamiento
%     pos_m1 = val(1,i);
%     x_train(i,:) = out(pos_m1,:);
% end
% 
% %% MARTIZ TEST %%%
% for i=entrenamiento+1:m
%     pos_m1 = val(1,i);
%     x_test(i-entrenamiento,:) = out(pos_m1,1:13); 
% end
% matr = struct('train', x_train, 'test', x_test);
% matrizCaracteristicas = matr;
end
 