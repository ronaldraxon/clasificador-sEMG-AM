function [ output_args ] = probarEscenariosSugeno(entradas,salidas,camposAUtilizar,escenario)
%PROBARESCENARIOS Summary of this function goes here
%%                              
%%[ output_args ] = set;                              
cantidadFinalDeClusters = 135;
entradas = entradas(:,camposAUtilizar);
tamanio = size(entradas);
conjuntoFinal = [];
    for i = 5:5:cantidadFinalDeClusters
        resultado = entrenamientoFISSugeno(entradas,salidas, i);
        fila = horzcat(escenario,i,1,resultado);
        set = fila;
        for j = 1:9
            resultado = entrenamientoFISSugeno(entradas,salidas, i);
            fila = horzcat(escenario,i,1,resultado);
            set = vertcat(set,fila);
        end
        conjuntoFinal = vertcat(conjuntoFinal,set);
    end
     resultado = entrenamientoFISSugeno(entradas,salidas, tamanio(1));
     fila = horzcat(escenario,tamanio(1),1,resultado);
     set = fila;
     conjuntoFinal = vertcat(conjuntoFinal,set);
[ output_args ] = conjuntoFinal;
end

