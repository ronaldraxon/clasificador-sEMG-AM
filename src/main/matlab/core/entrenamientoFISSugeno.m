function  output_args = entrenamientoFISSugeno( entradas,salidas, cantidadDeClusters)
%ENTRENAMIENTOFIS Summary of this function goes here
%   Detailed explanation goes here
opt = genfisOptions('FCMClustering','FISType','sugeno');
opt.NumClusters = cantidadDeClusters;
opt.Verbose = 0;
fis = genfis(entradas,salidas,opt);
output = evalfis(entradas,fis);
tamanio = size(salidas);
result = 0;  
for i = 1:tamanio(1) 
    sal1 = 0;
    
    sal1Prima = salidas(i,1);
    
    if output(i,1) > 0.5 
        sal1 = 1;
    end
    
    if (sal1 == sal1Prima)
        result = result + 1;
    end
end
output_args = result;
end

